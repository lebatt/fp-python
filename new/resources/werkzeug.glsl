#version 330

#if defined VERTEX_SHADER

in vec3 in_vert;
uniform mat4 m_proj;
uniform mat4 m_modelview;

void main() {
    gl_Position = vec4(in_vert, 1.0);
}

#elif defined FRAGMENT_SHADER

out vec4 f_color;

void main() {
    f_color = vec4(0.30, 0.50, 1.00, 1.0);
}

#endif

