from pathlib import Path
from typing import Sequence, Tuple
from array import array

import moderngl
import numpy as np
import math
from moderngl.program_members import varying
from pyrr.matrix44 import inverse
from moderngl_window import geometry
from base import CameraWindow
from pyrr import Matrix44, Matrix33
from moderngl_window.opengl.vao import VAO
import math

class Cylinder(CameraWindow):

    SEGMENT_COUNT = 36
    def __init__(self, ctx: moderngl.Context, offset: Tuple[int, int, int]):
        n = self.SEGMENT_COUNT
        self.ctx = ctx
        self.offset = offset

        # create vertices
        t = 2 * math.pi * np.arange(0, n) / n
        circle = np.array([np.cos(t), np.sin(t), np.ones_like(t) * -0.5]).transpose()
        vertices = np.vstack((circle, circle, np.array([(-0.5, -0.5, -0.5), (0.5, 0.5, 0.5)])))
        vertices[n:-2, 2] = 0.5

        # create faces
        f = []
        for i in range(n):
            f.append([i, (i + 1) % n, i + n])  # vertical face 1
            f.append([i + n, (i + 1) % n + n, (i + 1) % n])  # vertical face 2
            f.append([i, (i + 1) % n, 2 * n])  # bottom plate
            f.append([i + n, (i + 1) % n + n, 2 * n + 1])  # top plate
        faces = np.array(f)

        # Store vertices in homogeneous coordinate
        self._vertices = np.hstack(
            (np.array(vertices, dtype=np.double), np.ones((len(vertices), 1)))
        )
        self._faces = np.reshape(np.array(faces, dtype=np.uint32), (len(faces), 3))

        # transfrom and animate
        self.transform = self.ctx.program(
            vertex_shader='''
            #version 330

            uniform vec3 Acc;

            in vec3 in_pos;
            in vec3 in_prev;

            out vec3 out_pos;
            out vec3 out_prev;

            void main() {
                vec3 velocity = in_pos - in_prev;
                out_pos = in_pos + velocity + Acc;
                out_prev = in_pos;
            }
        ''',
            varyings=['out_pos', 'out_prev']
        )

if __name__ == '__main__':
    moderngl_window.run_window_config(Cylinder)