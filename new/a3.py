import numpy
import math

class Tool:
    def __init__(self, no, position, v, dz):
        self.no = no
        self.position = position
        self.v = v
        self.dz = dz

class A3:
    def __init__(self, velocity, max_velocity, data_path): 
        self.data = numpy.genfromtxt(data_path, delimiter=' ')
        #print(data)

        self.velocity = velocity
        self.max_velocity = max_velocity

        self.tools = []
    
    def runProcess(self):
        entries = self.mapData()
        process_time = 0

        for entry in entries:
            if self.toolIsActive(entry["toolNo"]):
                tool = self.getTool(entry["toolNo"])
                start = tool.position
                end = (entry["x"], entry["y"], entry["z"])
                process_time += self.calculateTime(start, end)
                self.updateToolPostion(entry["toolNo"], end)
                print("{} {} {} {} {} {}".format(tool.position[0], tool.position[1], tool.position[2], tool.v, tool.dz, tool.no ))
            else:
                pos = self.getPosition(entry["x"], entry["y"], entry["z"])
                self.addTool(entry["toolNo"], pos, entry["v"], entry["dz"])
        
        print("Process ended. estimated time: {}s".format(process_time))

    def updateToolPostion(self, toolNo, position):
        for tool in self.tools:
            if tool.no == toolNo:
                tool.position = position

    def toolIsActive(self, toolNo):
        for tool in self.tools:
            if tool.no == toolNo:
                return True
        
        return False

    def addTool(self, toolNo, position, v, dz):
        tool = Tool(toolNo, position, v, dz)
        self.tools.append(tool)
        print("{} {} {} {} {} {}".format(tool.position[0], tool.position[1], tool.position[2], tool.v, tool.dz, tool.no ))

    def getTool(self, toolNo):
        for tool in self.tools:
            if tool.no == toolNo:
                return tool

    def getPosition(self, x, y, z):
        return (x, y, z)
    
    def mapData(self):
        entries = []
        for entry in self.data:
            entries.append({
                "x": entry[0],
                "y": entry[1],
                "z": entry[2],
                "v": entry[3], # velocity
                "dz": entry[4], # drehzahl
                "toolNo": entry[5]
            })

        return entries

    def calculateTime(self, start, end):
        dist = math.dist(start, end)
        time = 0

        while (dist > 0):
            if( self.velocity <= self.max_velocity ):
                dist -= self.velocity
            else:
                dist -= self.max_velocity
            time += 1

        return time

if __name__ == "__main__":
    instance = A3(3, 6, "new/resources/data.csv")
    instance.runProcess()