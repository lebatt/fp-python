from pyrr import Matrix44

import moderngl
import moderngl_window
from moderngl_window import geometry

class Cube: 
    def __init__(self, ctx, cube, x, y, z, s, wnd):
        self.x = x
        self.y = y
        self.z = z
        self.show = s

        shader_source = {
            'vertex_shader': '''
                #version 330

                in vec3 in_position;
                in vec3 in_normal;

                uniform vec4 pos_offset;

                uniform Projection {
                    uniform mat4 matrix;
                } proj;

                uniform View {
                    uniform mat4 matrix;
                } view;

                out vec3 normal;
                out vec3 pos;
                out float show;

                void main() {
                    vec4 p = view.matrix * vec4(in_position + pos_offset.xyz, 1.0);
                    gl_Position =  proj.matrix * p;
                    mat3 m_normal = transpose(inverse(mat3(view.matrix)));
                    normal = m_normal * in_normal;
                    pos = p.xyz;
                    show = pos_offset.w;
                }
            ''',
            "geometry_shader": '''
                #version 330

                uniform sampler2D lookup;

                in float show[];
                in vec3 normal[];

                out vec3 pos;
                out vec3 out_normal;

                layout(points) in;
                layout(points, max_vertices = 1) out; 

                void main() {
                    if (show[0] >= 1.0) {
                        gl_Position = gl_in[0].gl_Position;
                        out_normal = normal[0];
                        pos = gl_in[0].gl_Position.xyz;
                        EmitVertex();
                        EndPrimitive();
                    }
                }
            ''',
            'fragment_shader': '''
                #version 330

                out vec4 color;

                in vec3 normal;
                in vec3 pos;

                void main() {
                    float l = dot(normalize(-pos), normalize(normal));
                    color = vec4(1.0) * (0.25 + abs(l) * 0.75);
                }
            ''',
        }
        self.prog = ctx.program(**shader_source)

        #self.prog = prog
        self.wnd = wnd
        self.prog['pos_offset'].value = (x, y, z, self.show)
        self.vao = cube.instance(self.prog)
        self.proj_uniform = self.prog['Projection']
        self.view_uniform = self.prog['View']
        self.m_proj = Matrix44.perspective_projection(
            75, self.wnd.aspect_ratio,  # fov, aspect
            0.1, 100.0,  # near, far
            dtype='f4',
        )
        self.proj_buffer = ctx.buffer(reserve=self.proj_uniform.size)
        self.view_buffer = ctx.buffer(reserve=self.view_uniform.size)
        self.proj_uniform.binding = 1
        self.view_uniform.binding = 2
        self.proj_buffer.write(self.m_proj.tobytes())
        self.scope = ctx.scope(
            ctx.fbo,
            enable_only=moderngl.CULL_FACE | moderngl.DEPTH_TEST,
            uniform_buffers=[
                (self.proj_buffer, 1),
                (self.view_buffer, 2),
            ],
        )

class Quader(moderngl_window.WindowConfig):
    title = "Quader"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.unit = 0.3
        self.cubeGeometry = geometry.cube(size=(self.unit, self.unit, self.unit), uvs=True)
        self.hoehe = 5
        self.breite = 5
        self.laenge = 5
        
        self.cubes = []
        offsetX = 0
        offsetY = 0
        offsetZ = 0
        for x in range(self.laenge): 
            for y in range(self.breite): 
                for z in range(self.hoehe):
                    #if self.showCube(x, y, z): 
                    cube = Cube(self.ctx, self.cubeGeometry, offsetX, offsetY, offsetZ, 1.0, self.wnd)
                    #if (self.showCube(offsetX, offsetY, offsetZ)):
                    self.cubes.append(cube)
                    offsetZ += self.unit
                offsetZ = 0
                offsetY += self.unit
            offsetY = 0
            offsetX += self.unit

        #self.prog1 = self.ctx.program(**shader_source)
        #self.prog1['pos_offset'].value = (1.1, 0, 0)
        #self.prog2 = self.ctx.program(**shader_source)
        #self.prog2['pos_offset'].value = (-0.9, 0, 0)

        #self.vao1 = self.cube.instance(self.prog1)
        #self.vao2 = self.cube.instance(self.prog2)

        #self.vaos = [self.vao1, self.vao2]

        #self.m_proj = Matrix44.perspective_projection(
         #   75, self.wnd.aspect_ratio,  # fov, aspect
          #  0.1, 100.0,  # near, far
           # dtype='f4',
        #)

        #proj_uniform1 = self.prog1['Projection']
        #view_uniform1 = self.prog1['View']
        #proj_uniform2 = self.prog2['Projection']
        #view_uniform2 = self.prog2['View']

        #self.proj_buffer = self.ctx.buffer(reserve=proj_uniform1.size)
        #self.view_buffer = self.ctx.buffer(reserve=view_uniform1.size)

        #proj_uniform1.binding = 1
        #view_uniform1.binding = 2
        #proj_uniform2.binding = 1
        #view_uniform2.binding = 2

        #self.proj_buffer.write(self.m_proj.tobytes())

        #self.scope1 = self.ctx.scope(
        #    self.ctx.fbo,
        #    enable_only=moderngl.CULL_FACE | moderngl.DEPTH_TEST,
        #    uniform_buffers=[
        #        (self.proj_buffer, 1),
        #        (self.view_buffer, 2),
        #    ],
        #)

        #self.scope2 = self.ctx.scope(
        #    self.ctx.fbo,
        #    enable_only=moderngl.CULL_FACE | moderngl.DEPTH_TEST,
        #    uniform_buffers=[
        #        (self.proj_buffer, 1),
        #        (self.view_buffer, 2),
        #    ],
        #)

        #self.scopes = [self.scope1, self.scope2]
        self.rotation = Matrix44.from_eulers((1, 2, 0), dtype='f4')

    def render(self, time=0.0, frametime=0.0, target: moderngl.Framebuffer = None):
        self.ctx.clear(0.2, 0.2, 0.2)
        self.ctx.enable(moderngl.BLEND)

        self.rotation = Matrix44.from_eulers((time, time, time), dtype='f4')
        translation = Matrix44.from_translation((0.0, 0.0, -5.0), dtype='f4')
        modelview = translation * self.rotation
        
        for cube in self.cubes:
            with cube.scope:
                cube.view_buffer.write(modelview)
                cube.vao.render(moderngl.LINE_STRIP)

    def mouse_drag_event(self, x, y, dx, dy):
        print("Mouse drag pos={} {} delta={} {}".format(x, y, dx, dy))

    
    def showCube(self, x, y, z) :
        for cube in self.cubes: 
            print(cube.x)
            if(cube.x == x and cube.y == y and cube.z + self.unit == z and not cube.show):
                self.setShowCube(x, y, z, True)
                return True
            
            if(cube.x == x and cube.y == y and cube.z - self.unit == z and not cube.show):
                self.setShowCube(x, y, z, True)
                return True

            if(cube.x == x and cube.y + self.unit == y and cube.z == z and not cube.show):
                self.setShowCube(x, y, z, True)
                return True

            if(cube.x == x and cube.y - self.unit == y and cube.z == z and not cube.show):
                self.setShowCube(x, y, z, True)
                return True

            if(cube.x + self.unit == x and cube.y == y and cube.z == z and not cube.show):
                self.setShowCube(x, y, z, True)
                return True

            if(cube.x - + self.unit== x and cube.y == y and cube.z == z and not cube.show):
                self.setShowCube(x, y, z, True)
                return True

            return False

    def setShowCube(self, x, y, z, value):
        for index in range(len(self.cubes)):
            if (x == self.cubes[index].x and y == self.cubes[index].y and z == self.cubes[index].z) :
                self.cubes[index].show = value
        
        return False
if __name__ == '__main__':
    moderngl_window.run_window_config(Quader)
