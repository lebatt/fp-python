### What is this repository for? ###


### How do I get set up? ###

## windows

- install python https://phoenixnap.com/kb/how-to-install-python-3-windows
- install visual c++ build tools https://visualstudio.microsoft.com/visual-cpp-build-tools/
- clone repository
- install miniconda https://docs.conda.io/projects/conda/en/latest/user-guide/install/windows.html

## linux