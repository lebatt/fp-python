import numpy as np
from renderer import Renderer
from shaders import *
import moderngl
import colorsys
import math

class Cube: 
    def __init__(self, x, y, z, id):
        self.id = id
        self.x = x
        self.y = y
        self.z = z

    def getVertices(self):
        return [

        ]

class TestRenderer(Renderer):

    def main(self):
        self.prog = self.ctx.program(vertex_shader=vertex_shader_source_unlit, fragment_shader=fragment_shader_source_unlit)
        self.prog_mvp = self.prog['modelViewProj']
        self.prog_color = self.prog['color']
        
        x = 0.8
        y = 0.8
        z = 0.8

        cubes =[]

        ## add cubes on x-axis
        for i in range(5) :
            cubes.append([
                [[-x + x + 0.1, -y, -z],               
                [x + x + 0.1, -y, -z],
                [x + x + 0.1,  y, -z],
                [x + x + 0.1,  y, -z],
                [-x + x + 0.1,  y, -z],
                [-x + x + 0.1, -y, -z]],# one face faceid 2

                [[-x + x + 0.1, -y,  z],#6
                [x + x + 0.1, -y,  z],
                [x + x + 0.1,  y,  z],
                [x + x + 0.1,  y,  z],
                [-x + x + 0.1,  y,  z],
                [-x + x + 0.1, -y,  z]],#two face   faceid 5

                [[-x + x + 0.1,  y,  z],#12
                [-x + x + 0.1,  y, -z],
                [-x + x + 0.1, -y, -z],
                [-x + x + 0.1, -y, -z],
                [-x + x + 0.1, -y,  z],
                [-x + x + 0.1,  y,  z]],#three face  faceid4

                [[x + x + 0.1,  y,  z],#18
                [x + x + 0.1,  y, -z],
                [x + x + 0.1, -y, -z],
                [x + x + 0.1, -y, -z],
                [x + x + 0.1, -y,  z],
                [x + x + 0.1,  y,  z]],#four face   faceid0

                [[-x + x + 0.1, -y, -z],#24
                [x + x + 0.1, -y, -z],
                [x + x + 0.1, -y,  z],
                [x + x + 0.1, -y,  z],
                [-x + x + 0.1, -y,  z],
                [-x + x + 0.1, -y, -z]],# five face faceid 3

                [[-x + x + 0.1,  y, -z],
                [x + x + 0.1,  y, -z],
                [x + x + 0.1,  y,  z],
                [x + x + 0.1,  y,  z],
                [-x + x + 0.1,  y,  z],
                [-x + x + 0.1,  y, -z]],# six + x + 0.1 face faceid 1
            ]
            )
            
            x += 0.1

        stack = np.vstack(cubes)
        vertices = np.array(stack,dtype='f4')

        self.vbo = self.ctx.buffer(vertices)
        self.vao = self.ctx.simple_vertex_array(self.prog, self.vbo, 'position')
        self.hue = 0.0

        self.run()

    def on_draw(self, ctx, dt):
        #self.hue = math.fmod(self.hue + dt * x, 1.0)
        color = (254, 254, 254)

        self.prog_mvp.write(self.modelViewProjection.astype('f4').tobytes())
        self.prog_color.write(np.array(color, dtype='f4').tobytes())
        self.vao.render(moderngl.TRIANGLES)
if __name__ == "__main__":
    TestRenderer().main()
